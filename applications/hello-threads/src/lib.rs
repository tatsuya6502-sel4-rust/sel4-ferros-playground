#![no_std]

use core::fmt;
use ferros::cap::{role, CNodeRole};
use ferros::userland::{Caller, Responder, RetypeForSetup};
use ferros::vspace::{shared_status, MappedMemoryRegion};

#[derive(Debug, Clone, PartialEq)]
pub enum Request {
    Start,
}

impl fmt::Display for Request {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Request::Start => write!(f, "Start"),
        }
    }
}

#[allow(clippy::large_enum_variant)]
#[derive(Debug, Clone, PartialEq)]
pub enum Response {
    Started,
}

impl fmt::Display for Response {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Response::Started => write!(f, "Started"),
        }
    }
}

#[repr(C)]
pub struct ProcParams<Role: CNodeRole> {
    pub num_threads: u8,
    pub responder: Responder<Request, Result<Response, ()>, Role>,
}

impl RetypeForSetup for ProcParams<role::Local> {
    type Output = ProcParams<role::Child>;
}
