#![no_std]
#![no_main]

use selfe_runtime as _;

use hello_threads::{ProcParams, Request, Response};
use core::convert::TryInto;
use core::hash::{Hash, Hasher};
use core::str;
use debug_logger::DebugLogger;
use ferros::cap::role;

static LOGGER: DebugLogger = DebugLogger;

#[allow(improper_ctypes_definitions)]
#[no_mangle]
pub extern "C" fn _start(params: ProcParams<role::Local>) -> ! {
    log::set_logger(&LOGGER)
        .map(|()| log::set_max_level(DebugLogger::max_log_level_from_env()))
        .unwrap();

    log::debug!("[hello-threads] Process started",);
    log::debug!("[hello-threads] num_threads={}", params.num_threads,);

    params
        .responder
        .reply_recv(move |req| {
            log::debug!("[hello-threads] Processing request {}", req);
            let resp = match req {
                Request::Start => Ok(Response::Started),
            };
            if let Ok(r) = &resp {
                log::debug!("[hello-threads] Response {}", r);
            } else {
                log::debug!("[hello-threads] Response {:?}", resp);
            }
            resp
        })
        .expect("Could not set up a reply_recv")
        .expect("Failure on reply_recv");

    unsafe {
        loop {
            selfe_sys::seL4_Yield();
        }
    }
}
